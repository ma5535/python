# Normalna porazdelitev

[Normalna porazdelitev](https://sl.wikipedia.org/wiki/Normalna_porazdelitev): $X\sim N(\mu, \sigma)$
 - $\mu$ je enak pričakovani vrednosti $E(X)$
 - $\sigma$ je enak standardnemu odklonu $ \sqrt{D(X)}$

## Standardizacija

Porazdelitev $N(0,1)$ imenujemo **standardna normalna porazdelitev**. Porazdelitev je zvezna, zato je določena z eno od dveh funkcij
  - **[gostota verjetnosti](https://sl.wikipedia.org/wiki/Gostota_verjetnosti)**
   $$ \frac {1}{\sqrt{2\pi}}e^{-\frac{x^2}{2}}$$
  - **[kumulativna porazdelitvena funkcija](https://sl.wikipedia.org/wiki/Zbirna_funkcija_verjetnosti)**
  $$ P(X\le x) = \Phi(x) = \frac{1}{2\pi}\int_{-\infty}^x e^{-\frac{t^2}{2}}dt $$
  
Kumulativna porazdelitvena funkcija $\Phi(x)$ ni elementarna in jo računamo s [tabelo](Standardna_normalna_porazdelitev.pdf) ali z uporabo knjižnice `scipy.stats`.

Vsako normalno porazdeljeno slučajno spremenljivko $X(\mu, \sigma)$ lahko **standardiziramo**

 $$ Z = \frac{X-\mu}{\sigma}\sim N(0,1),$$
 
 in dobimo slučajno spremenljivko $Z\sim N(0,1)$, ki je porazdeljena standardno normalno.


```python
from scipy.stats import norm
print("P(-1 < Z < 1) = {}".format(norm.cdf(1)-norm.cdf(-1)))
```

    P(-1 < Z < 1) = 0.6826894921370859


*Opomba: Če ni na voljo knjižnice `scipy`, lahko $\Phi$ definiramo s funkcijo napake `math.erf` iz standardne matematične knjižnice.*


```python
from math import erf
def norm_cdf(x):
    "kumulativna porazdelitvena funkcija za N(0,1)"
    return 0.5 + 0.5*erf(x/2**0.5)
```

## Naloga
Za slučajno spremenljivko $X\sim N(\mu, \sigma)$ izračuaj
 1. $P(X<3/2)$, če je $\mu=0$ in $\sigma=1$
 2. $P(X>0)$, če je $\mu=-9$ in $\sigma=5$
 3. $P(0<X<3)$, če je $\mu=2$ in $\sigma=2$.

$P(X<3/2) = P(X\le 3/2) = \Phi(3/2)$ 


```python
from math import erf
def norm_cdf(x):
    "kumulativna porazdelitvena funkcija za N(0,1)"
    return 0.5 + 0.5*erf(x/2**0.5)
```


```python
norm_cdf(3/2)
```




    0.9331927987311419




```python
# kumulativna porazdelitvena funkcija za N(0,1)
norm.cdf(3/2)
```




    0.93319279873114191




```python
3/2
```




    1.5



$Z = (X + 9)/5$

$P(X>0) = P(Z>(0+9)/5) = P(Z>9/5) = 1 - P(Z\le 9/5)$


```python
1-norm.cdf(9/5)
```




    0.035930319112925768




```python
1- 0.9641
```




    0.03590000000000004



$X\sim N(2,2)$

$P(0<X<3) = P(-1<Z<1/2) = \Phi(1/2) - \Phi(-1)$


```python
norm.cdf(0.5) - norm.cdf(-1)
```




    0.53280720734255604




```python
0.6915 - (1 - 0.8413)
```




    0.5328



## Naloga
Telesna višina žensk je $X\sim N(165, 7)$, telesna višina moških je $Y\sim N(178,9)$
 - Delež moških nad 190
 - Delež moških pod povprečjem žensk in žensk nad povprečjem moških
 - razmerje med deležem moških nad 185 in žensk nad 185


```python
from scipy.stats import norm
```


```python
# verjetnost P(Y>190) = 1 - P(Z>(190-178)/9) = 1 - Phi(12/9)
1 - norm.cdf((190-178)/9)
```




    0.091211219725867876



Delež moških pod povprečno višino žensk
$$P(Y<165) = P(Z_Y <(165-178)/9) = \Phi(-13/9)$$


```python
print("Delež moških, ki so manjši od povprečne ženske je {}".format(norm.cdf(-13/9)))
```

    Delež moških, ki so manjši od povprečne ženske je 0.07430699822760586


Delež žensk, ki so večje od povprečnega moškega
$$ P(X>178) = 1 - P(X\le 178) = 1 - \Phi((178 - 165)/7) $$


```python
print("Delež žensk, ki so večje od povprečnega moškega je {}".format(1-norm.cdf(13/7)))
```

    Delež žensk, ki so večje od povprečnega moškega je 0.0316454161166726


Razmerje med deležem moških, ki so večji od 185 in deležem žensk, ki so večje od 185 je enako razmerju verjetnosti
$$P(Y>185)/P(X>185)$$
$$P(Y>185) = 1 - P(Y\le 185) = 1 - \Phi((185 -178)/9)$$


```python
Px = 1 - norm.cdf((185 - 178)/9)
Py = 1 - norm.cdf((185 - 165)/7)
Px/Py
```




    102.15841144535989




```python
(1+4+4+9+16+36)
```




    70




```python
from math import erf
def norm_cdf(x):
    "kumulativna porazdelitvena funkcija za N(0,1)"
    return 0.5 + 0.5*erf(x/2**0.5)
```

Standardizacija za $X$ je dana s formulo
$$Z_X = \frac{X-165}{7}$$
Standardizacija za $Y$ pa s formulo
$$Z_Y = \frac{Y-178}{9}$$



```python
# standardizacija
Zz = lambda x: (x - 165)/7
Zm = lambda y: (y - 178)/9
```

$P(Y>190) = P(Z> (190 - 178)/9)= 1- \Phi(12/9)$


```python
Pa = 1 - norm_cdf((190-178)/9)
print("Delež moških nad 190 je enak {}".format(Pa))
```

    Delež moških nad 190 je enak 0.09121121972586788


$P(Y<165)=P(Z_m < (165 - 178)/9)$


```python
Pbm = norm_cdf(Zm(165))
print("standardizirana vrednost 165cm je {}".format(Zm(165)))
print("delež moških manjših od povprečne ženske je {}".format(Pbm))
```

    standardizirana vrednost 165cm je -1.4444444444444444
    delež moških manjših od povprečne ženske je 0.07430699822760606

